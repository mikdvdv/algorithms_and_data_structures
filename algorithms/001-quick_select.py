from random import randint

def k_is_not_valid(arr: list, k: int) ->  bool:
    arr_len = len(arr)
    if k <= 0 or arr_len < k:
        return True
    else:
        return False

def partition(arr: list, k: int, startpoint: int = 0, endpoint: int = -1) -> int:
    if endpoint == -1:
        # Probably we don't need arr_len anywhere else
        arr_len = len(arr)
        endpoint = arr_len - 1
        # Set the seed if we are on the top of the call stack
        #seed(1967)
    # Set random pivot
    pivot_position = randint(startpoint, endpoint)
    pivot = arr[pivot_position]
    position_for_smalls = startpoint - 1
    # Put elements less than or equal to pivot to the left
    for position, element in enumerate(arr[startpoint: endpoint + 1]):
        position += startpoint
        # If we encounter pivot, we are going to move it, so we need to refresh it's position
        if position == pivot_position:
            pivot_position = position_for_smalls + 1
        #
        if element <= pivot:
            position_for_smalls += 1
            arr[position_for_smalls], arr[position] = arr[position], arr[position_for_smalls]
    # Put the pivot to it's place 
    arr[pivot_position], arr[position_for_smalls] = arr[position_for_smalls], arr[pivot_position]
    # Refresh the pivot's position
    pivot_position = position_for_smalls
    return pivot_position

def quick_select_min(arr: list, k: int, startpoint: int = 0, endpoint: int = -1) -> int:
    # It is better to put validation outside of the function, so we don't repeat it over and over again
    if k_is_not_valid(arr, k):
        return -1
    pivot_position = partition(arr, k, startpoint, endpoint)
    if pivot_position == k - 1:
        return arr[pivot_position]
    elif pivot_position > k - 1:
        return quick_select_min(arr, k, startpoint, pivot_position - 1)
    elif pivot_position < k - 1:
        return quick_select_min(arr, k, pivot_position + 1, endpoint)

def quick_select_max(arr: list, k: int, startpoint: int = 0, endpoint: int = -1) -> int:
    # It is better to put validation outside of the function, so we don't repeat it over and over again
    if k_is_not_valid(arr, k):
        return -1
    arr_len = len(arr)
    pivot_position = partition(arr, k, startpoint, endpoint)
    if pivot_position == arr_len - k:
        return arr[pivot_position]
    elif pivot_position > arr_len - k:
        return quick_select_max(arr, k, startpoint, pivot_position - 1)
    elif pivot_position < arr_len - k:
        return quick_select_max(arr, k, pivot_position + 1, endpoint)


def main():
    print(quick_select_max([7, 10, 4, 3, 20, 15], 0))
    print(quick_select_max([7, 10, 4, 3, 20, 15], 7))
    print(quick_select_max([7, 10, 4, 3, 20, 15], 6))
    print(quick_select_max([7, 10, 4, 3, 20, 15], 3))
    print(quick_select_min([7, 10, 4, 3, 20, 15], 0))
    print(quick_select_min([7, 10, 4, 3, 20, 15], 7))
    print(quick_select_min([7, 10, 4, 3, 20, 15], 6))
    print(quick_select_min([7, 10, 4, 3, 20, 15], 3))
    pass

if __name__ == "__main__":
    main()