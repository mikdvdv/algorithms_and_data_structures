from random import randint, seed

def quicksort_rec(arr: list, startpoint: int = 0, endpoint: int = -1):
    arr_len = len(arr)
    if arr_len <= 1:
        return None
    if endpoint == -1:
        endpoint = arr_len - 1
        # Set the seed if we are on the top of the call stack
        #seed(1967)
    # Set random pivot
    pivot_position = randint(startpoint, endpoint)
    pivot = arr[pivot_position]
    position_for_smalls = startpoint - 1
    # Put elements less than or equal to pivot to the left
    for position, element in enumerate(arr[startpoint: endpoint + 1]):
        position += startpoint
        # If we encounter pivot, we are going to move it, so we need to refresh it's position
        if position == pivot_position:
            pivot_position = position_for_smalls + 1
        #
        if element <= pivot:
            position_for_smalls += 1
            arr[position_for_smalls], arr[position] = arr[position], arr[position_for_smalls]
    # Put the pivot to it's place 
    arr[pivot_position], arr[position_for_smalls] = arr[position_for_smalls], arr[pivot_position]
    # Refresh the pivot's position
    pivot_position = position_for_smalls
    # Sort the array to the left of the pivot if needed
    if pivot_position != startpoint:
        quicksort_rec(arr, startpoint, pivot_position - 1)
    # Sort the array to the right of the pivot if needed
    if pivot_position != endpoint:
        quicksort_rec(arr, pivot_position + 1, endpoint)

    


def main():
    pass



if __name__ == "__main__":
    main()