class Ordered_Doubly_Linked_List:
    class Node:
        def __init__(self, value: int):
            self.val = value
            self.next = None
            self.prev = None
    def __init__(self, value: int, capacity: int, pop_heads: bool = True):
        first_node = self.Node(value)
        self.head = first_node
        self.tail = first_node
        self.capacity = capacity
        self.amount = 1
        self.pop_heads = pop_heads
    def __repr__(self):
        recent = self.head
        repr_str = ""
        while recent.next:
            repr_str += str(recent.val) + " -> "
            recent = recent.next
        repr_str += str(recent.val)
        return repr_str
    def append(self, value: int):
        new_node = self.Node(value)
        if new_node.val <= self.head.val:
            new_node.next = self.head
            self.head.prev = new_node
            self.head = new_node
        if new_node.val > self.tail.val:
            self.tail.next = new_node
            new_node.prev = self.tail
            self.tail = new_node
        if new_node.val > self.head.val and new_node.val < self.tail.val:
            recent = self.head
            while recent.next and recent.next.val < new_node.val:
                recent = recent.next
            new_node.next = recent.next
            recent.next.prev = new_node
            new_node.prev = recent
            recent.next = new_node
        self.amount += 1
        if self.amount > self.capacity:
            if self.pop_heads:
                self.pop_head()
            else:
                self.pop_tail()
            self.amount -= 1
        return None
    def pop_head(self):
        val = self.head.val
        self.head = self.head.next
        self.head.prev = None
        return val
    def pop_tail(self):
        val = self.tail.val
        self.tail = self.tail.prev
        self.tail.next = None
        return val
    def values(self):
        vals = []
        trans = self.head
        while trans:
            vals.append(trans.val)
            trans = trans.next
        return vals
    def sum(self):
        sum = 0
        trans = self.head
        while trans:
            sum += trans.val
            trans = trans.next
        return sum
