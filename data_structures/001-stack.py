class Stack:
    class Node:
        def __init__(self, value):
            self.val = value
            self.prev = None
        def __repr__(self):
            return str(self.val)
    def __init__(self, value = None):
        if value:
            self.head = self.Node(value)
        else:
            self.head = None
    def __repr__(self):
        if self.head == None:
            return ""
        trans = self.head
        repr_str = str(self.head.val)
        while trans.prev:
            repr_str += " -> " + str(trans.prev.val)
            trans = trans.prev
        return repr_str       
    def push(self, value):
        if self.head:
            new_node = self.Node(value)
            new_node.prev = self.head
            self.head = new_node
        else:
            self.head = self.Node(value)
        return None
    def pop(self):
        if self.head:
            val = self.head.val
            self.head = self.head.prev
            return val
        else:
            return None
    def glance(self):
        if self.head:
            return self.head.val
        else:
            return None