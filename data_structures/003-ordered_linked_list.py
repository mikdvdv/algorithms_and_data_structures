class Ordered_Linked_List:
    class Node:
        def __init__(self, value: int):
            self.val = value
            self.next = None
    def __init__(self, value: int):
        first_node = self.Node(value)
        self.head = first_node
        self.tail = first_node 
    def append(self, value: int):
        new_node = self.Node(value)
        if new_node.val <= self.head.val:
            new_node.next = self.head
            self.head = new_node
            return None
        elif new_node.val >= self.tail.val:
            self.tail.next = new_node
            self.tail = new_node
            return None
        else:
            recent = self.head
            while recent.next and recent.next.val < new_node.val:
                recent = recent.next
            new_node.next = recent.next
            recent.next = new_node
            return None
    def pop_head(self):
        val = self.head.val
        self.head = self.head.next
        return val
    def print(self):
        recent = self.head
        while recent.next:
            print(recent.val, " -> ", end = '')
            recent = recent.next
        print(recent.val)